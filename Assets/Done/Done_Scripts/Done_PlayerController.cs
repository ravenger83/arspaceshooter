﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Done_Boundary 
{
	public float xMin, xMax, yMin, yMax;
}

public class Done_PlayerController : MonoBehaviour
{
	public float speed;
	public float tilt;
	public float scale;
	public float controlSmoothing;
	public GameObject controlMarker;
	public GameObject gameWorld;
	public Done_Boundary boundary;

	public GameObject shot;
	public Transform shotSpawn;
	public float fireRate;
	 
	private float nextFire;

	void Start ()
	{
		// Move marker to player position.
		controlMarker.transform.position = transform.position;
	}
	
	void Update ()
	{
		if ((Input.GetButton("Fire1") || Input.GetKey(KeyCode.Return) || Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey((KeyCode)10)) && Time.time > nextFire) 
		{
			nextFire = Time.time + fireRate;
			((GameObject) Instantiate(shot, shotSpawn.position, shotSpawn.rotation)).transform.parent = gameWorld.transform;
			GetComponent<AudioSource>().Play ();
		}
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		bool moveToMarker = false;

		Vector3 movement = new Vector3 (moveHorizontal, moveVertical, 0.0f);
		if (movement == new Vector3 (0.0f, 0.0f, 0.0f)) {
			moveToMarker = true;
			Vector3 controlPos = controlMarker.transform.position;
			Vector3 currentPos = transform.position;
			// Move towards control box.
			if (controlPos.x != currentPos.x && controlPos.y != currentPos.y) {
				Vector3 moveTo = Vector3.MoveTowards(currentPos, controlPos, controlSmoothing * Time.deltaTime);
				moveTo.z = 0.0f;
				transform.position = moveTo;
			}
		}


		GetComponent<Rigidbody>().velocity = movement * speed * scale;

		GetComponent<Rigidbody>().position = new Vector3
		(
				Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin + gameWorld.transform.position.x, boundary.xMax + gameWorld.transform.position.x), 
				Mathf.Clamp (GetComponent<Rigidbody>().position.y, boundary.yMin+ gameWorld.transform.position.y, boundary.yMax + gameWorld.transform.position.y),
            0.0f
		);

		if (!moveToMarker) {
			// Move marker to ship position
			controlMarker.transform.position = transform.position;
		}

		GetComponent<Rigidbody>().rotation = Quaternion.Euler (-90.0f, GetComponent<Rigidbody>().velocity.x * -tilt, 0.0f);
	}
}

