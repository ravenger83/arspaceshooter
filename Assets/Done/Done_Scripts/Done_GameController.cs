﻿using UnityEngine;
using System.Collections;

public class Done_GameController : MonoBehaviour
{
	public GameObject[] hazards;
	public GameObject gameWorld;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public float scale = 1.0f;
	
	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	public GameObject background1;
	public GameObject background2;

	public GameObject [] gameBoards;

	private GameObject currentBoard;
	private int board = -1;

	private bool gameOver;
	private bool restart;
	private int score;
	
	void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		LoadGameboard (0);
	}

	void LoadGameboard(int i) 
	{
		if (this.board == i) {
			return;
		}
		this.board = i;
		Destroy (currentBoard);
		GameObject board = gameBoards [i];
		Vector3 spawnPosition = new Vector3(0,0,0);
		spawnPosition += gameWorld.transform.position;
		Quaternion spawnRotation = Quaternion.identity;
		currentBoard = (GameObject)Instantiate (board, spawnPosition, spawnRotation);
		currentBoard.transform.parent = gameWorld.transform;
	}

	void Update ()
	{
		if (restart)
		{
			if (Input.GetKeyDown (KeyCode.R) || Input.GetButton("Fire1"))
			{
				Application.LoadLevel (Application.loadedLevel);
			}
		}
		// Determine which background is active.
		ARMarker bg1 = background1.GetComponent<ARTrackedObject>().GetMarker();
		if (bg1.Visible) {
			LoadGameboard (0);
		}
		ARMarker bg2 = background2.GetComponent<ARTrackedObject>().GetMarker();
		if (!bg1.Visible && bg2.Visible) {
			LoadGameboard (1);
		}
			
	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++)
			{
                int hazardType = Random.Range(0, hazards.Length);
                GameObject hazard = hazards [hazardType];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				spawnPosition.Scale (new Vector3 (scale, scale, scale));
				spawnPosition += gameWorld.transform.position;
                Quaternion spawnRotation = Quaternion.identity;
				((GameObject) Instantiate (hazard, spawnPosition, spawnRotation)).transform.parent = gameWorld.transform;
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
			
			if (gameOver)
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}
	
	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}
	
	void UpdateScore ()
	{
		scoreText.text = "Score: " + score;
	}
	
	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
	}
}